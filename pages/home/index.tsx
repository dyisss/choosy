import {NextPage} from "next";
import {Grid, Card, Text, Container, Row, Col, Image, Spacer} from "@nextui-org/react";
import Nav from "../nav";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faBatteryEmpty, faBridgeCircleCheck,
    faClock,
    faFireBurner,
    faPersonChalkboard,
    faTag,
    faTimes,
    faUtensils
} from "@fortawesome/free-solid-svg-icons";

// @ts-ignore
const MockItem = ({ text }) => {
    return (
        <Card color="primary" css={{ h: "$20" }}>
            <Text h6 size={15} color="white" css={{ m: 0 }}>
                {text}
            </Text>
        </Card>
    );
};

const Home: NextPage = () => {
    return (
        <Container justify='space-between' display='inline-flex' responsive={true}>
                <Row justify="center" align="center">
                    <Text h1 size={60}>
                        Choosy
                    </Text>
                </Row>
              <Spacer/>
            <Row>
                <Text h3>Trendy Recipes</Text>
            </Row>
            <Row>
                <Image src={'https://smaakmenutie.nl/wp-content/uploads/2020/02/Beef-pho-4.jpg'} />
                <Spacer/>
                <Image src={'https://mahatmarice.com/wp-content/uploads/2019/10/Jolloff-Rice-Vanessa-Bell-De-Su-Mama.jpg'}/>
            </Row>
            <Row>
                <Text h3>
                    Recently Search
                </Text>
            </Row>
            <Row justify='space-evenly' align={'center'} >
                {/*@ts-ignore*/}
                <Col align={'center'}>
                    <Image src={'https://mahatmarice.com/wp-content/uploads/2019/10/Jolloff-Rice-Vanessa-Bell-De-Su-Mama.jpg'}  />
                </Col>
                <Spacer/>
                <Col>
                    <Row justify={'center'}>
                        <Text h4 color={'$green800'}>Jollof Rice</Text>
                    </Row>
                    <Spacer/>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faTag}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Course
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                Main
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faTag}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Cuisine
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                African
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faTag}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Keyword
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                Rice Tomato
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faPersonChalkboard}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Prep Time
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                20 minutes
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faFireBurner}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Cook Time
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                40 minutes
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faClock}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Total Time
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                 60 minutes
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faUtensils}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Servings
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                Main
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faBatteryEmpty}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Calories
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                429kcal
                            </Text>
                        </Col>
                    </Row>
                    <Row justify={'center'} align={'center'}>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                <FontAwesomeIcon
                                    icon={faBridgeCircleCheck}
                                    style={{ fontSize: 21, color: "grey" }}
                                />
                                Author
                            </Text>
                        </Col>
                        {/*@ts-ignore*/}
                        <Col align={'center'}>
                            <Text>
                                Zulfy Dirna
                            </Text>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Nav/>
        </Container>
    )
}

export default Home
import {Card, CssBaseline} from "@nextui-org/react";
import {AppProps} from "next/app";
import Document, {Html, Head, Main, NextScript, DocumentContext} from 'next/document';
import {NextPageContext} from "next";

class MyDocument extends Document {
    static async getInitialProps(ctx: DocumentContext) {
        const initialProps = await Document.getInitialProps(ctx);
        return {
            ...initialProps,
            styles: [<>{initialProps.styles}</>]
        };
    }

    render() {
        return (
            <Html lang="en">
                <Head>{CssBaseline.flush()}</Head>
                <body>
                <Main />
                <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
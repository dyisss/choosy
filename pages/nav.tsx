import {Col, Link, Row} from "@nextui-org/react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleUser, faHouse, faSearch, faStar} from "@fortawesome/free-solid-svg-icons";

const Nav = () => {
    return (
        <Row align='center' justify='space-around'>
            {/*@ts-ignore*/}
            <Col align='center'>
                <Link href={'/home'}>
                    <FontAwesomeIcon
                        icon={faHouse}
                        style={{ fontSize: 21, color: "grey" }}
                    />
                </Link>
            </Col>
            {/*@ts-ignore*/}
            <Col align='center' >
                <Link href={'/search'}>
                    <FontAwesomeIcon
                        icon={faSearch}
                        style={{ fontSize: 21, color: "grey" }}
                    />
                </Link>
            </Col>
            {/*@ts-ignore*/}
            <Col align='center' >
                <Link href={'/favorite'}>
                    <FontAwesomeIcon
                        icon={faStar}
                        style={{ fontSize: 21, color: "grey" }}
                    />
                </Link>
            </Col>
            {/*@ts-ignore*/}
            <Col align='center'>
                <Link href={'/profile'}>
                    <FontAwesomeIcon
                        icon={faCircleUser}
                        style={{ fontSize: 21, color: "grey" }}
                    />
                </Link>
            </Col>
        </Row>
    )
}

export default Nav;
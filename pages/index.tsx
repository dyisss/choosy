import {NextPage} from "next";
import {Grid, Card, Text, Container, Button} from "@nextui-org/react";
import { Link } from "@nextui-org/react";

// @ts-ignore
const MockItem = ({ text }) => {
    return (
        <Card color="primary" css={{ h: "$20" }}>
            <Text h6 size={15} color="white" css={{ m: 0 }}>
                {text}
            </Text>
        </Card>
    );
};

const Welcome: NextPage = () => {
    return (
        <Grid.Container justify='space-between' alignContent='space-between'>
            <Container>
                <Text h1 size={75}>Welcome to Choosy!</Text>
                <Text h1 size={50} css={{
                    color: '$green800'}}

                >{/* eslint-disable-next-line react/no-unescaped-entities */}
                    Let's get started!</Text>
            </Container>
            <Grid.Container gap={2} justify='center' direction='row'>
                <Grid>
                    <Link href='/home'>
                        <Button size='xl' css={{background: '$green800'}}>
                            Register
                        </Button>
                    </Link>
                </Grid>
                <Grid>
                    <Link href='/home'>
                        <Button size='xl' css={{background: 'black'}}>
                            Login
                        </Button>
                    </Link>
                </Grid>
            </Grid.Container>
        </Grid.Container>
    )
}

export default Welcome
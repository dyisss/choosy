import {NextPage} from "next";
import {Container, Image, Row, Spacer, Text} from "@nextui-org/react";
import Nav from "../../nav";

const LanguageElement = (prop:any) => {
    return(
        <Row justify='space-between'>
            <Text h4>
                {prop.language}
            </Text>
            <Image width={50} height={50} src={`${prop.flag}`}/>
            <div className="toggle-switch">
                <input type="checkbox" className="toggle-switch-checkbox" name="toggleSwitch" id="toggleSwitch"/>
                <label className="toggle-switch-label" htmlFor="toggleSwitch">
                    <span className="toggle-switch-inner"></span>
                    <span className="toggle-switch-switch"></span>
                </label>
            </div>
        </Row>
    )
}

const Language: NextPage = () => {
    return(
        <Container justify='space-between' display='inline-flex' responsive={true}>
            <Row align='center' justify='center' css={{ borderColor: "Grey",borderBottomStyle:"solid", borderBottomWidth: "thin"}}>
                <Text h1 size={55}> Choosy </Text>
            </Row>
            <Spacer/>
            <Row align='center' justify='center'>
                <Text h3>
                    Languages
                </Text>
            </Row>
            <Spacer/>
            <LanguageElement language='English' flag={'https://catamphetamine.gitlab.io/country-flag-icons/3x2/GB.svg'}></LanguageElement>
            <LanguageElement language='Dutch' flag={'https://catamphetamine.gitlab.io/country-flag-icons/3x2/NL.svg'}></LanguageElement>
            <LanguageElement language='German' flag={'https://catamphetamine.gitlab.io/country-flag-icons/3x2/DE.svg'}></LanguageElement>
            <LanguageElement language='French' flag={'https://catamphetamine.gitlab.io/country-flag-icons/3x2/FR.svg'}></LanguageElement>
            <LanguageElement language='Spanish' flag={'https://catamphetamine.gitlab.io/country-flag-icons/3x2/ES.svg'}></LanguageElement>
            <LanguageElement language='Russian' flag={'https://catamphetamine.gitlab.io/country-flag-icons/3x2/RU.svg'}></LanguageElement>
            <Nav></Nav>
        </Container>
    );
}

export default Language;
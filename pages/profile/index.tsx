import {NextPage} from "next";
import {Grid, Card, Text, Container, Row, Spacer, Input, Col, Avatar, Link} from "@nextui-org/react";
import styles from '../../styles/Home.module.css'
import Nav from "../nav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCircleQuestion,
    faCopy,
    faGear,
    faLanguage,
    faMessage,
    faShareFromSquare
} from "@fortawesome/free-solid-svg-icons";

const ProfileElement = (prop:any) => {
    return (
        <Row justify='space-between' align='center' css={{ borderColor: "Grey",borderTopStyle:"solid", borderTopWidth: "thin"}}>
            <Text h5 size={22}>{prop.text}</Text>
            <Spacer/>
            {prop.additionalType === 'text' && <Text size={21}>{prop.additional}</Text> }
            {prop.additionalType === 'icon' &&
                <Link href={`/profile/${prop.href}`}>
                    <FontAwesomeIcon
                        icon={prop.additional}
                        style={{ fontSize: 21, color: "grey" }}
                    />
                </Link>
                 }
        </Row>
    )
};

const Profile: NextPage = () => {
    return (
        <Container justify='space-between' display='inline-flex' responsive={true}>
            <Row align='center' justify='center' css={{ borderColor: "Grey",borderBottomStyle:"solid", borderBottomWidth: "thin"}}>
                <Text h1 size={55}> Choosy </Text>
            </Row>
            <Spacer/>
            <Col>
                <Row align='center' justify='center'>
                    <Avatar src='https://www.bakkerijtanthof.nl/wp-content/uploads/2019/02/doner-durum_1024x1024.jpg' size='xl'/>
                </Row>
                <Row align='center' justify='center'>
                    Manyak Groot Durum
                </Row>
            </Col>
            <Spacer/>
            <ProfileElement text='Email' additional='jmbchas@gmail.com' additionalType='text'/>
            <ProfileElement text='Age' additional='19' additionalType='text'/>
            <ProfileElement text='Allergy Setting' additional={faGear} additionalType='icon' href='allergy'/>
            <ProfileElement text='Languages' additional={faLanguage} additionalType='icon' href='language'/>
            <ProfileElement text='FAQ' additional={faCircleQuestion} additionalType='icon'/>
            <ProfileElement text='Contact us' additional={faMessage} additionalType='icon'/>
            <ProfileElement text='Share with a friend' additional={faShareFromSquare} additionalType='icon'/>
            <ProfileElement text='Copy Link' additional={faCopy} additionalType='icon'/>
            <Row align='center' justify='center'>
                <Text h5 color='$red500' size={22}>Delete Account</Text>
            </Row>
            <Row align='center' justify='center'>
                <Text h5 size={10}>1.0 Copyright 2021 Choosy</Text>
            </Row>
            <Nav></Nav>
        </Container>
    )
}

export default Profile
import {NextPage} from "next";
import {Container, Image, Input, Row, Spacer, Text} from "@nextui-org/react";
import Nav from "../../nav";

const AllergyElement = (prop:any) => {
    return(
        <Row justify='space-between'>
            <Text h4>
                {prop.food}
            </Text>
            <div className="toggle-switch">
                <input type="checkbox" value={1} className="toggle-switch-checkbox" name="toggleSwitch" id="toggleSwitch"/>
                <label className="toggle-switch-label" htmlFor="toggleSwitch">
                    <span className="toggle-switch-inner"></span>
                    <span className="toggle-switch-switch"></span>
                </label>
            </div>
        </Row>
    )
}


const Allergy: NextPage = () => {
    return(
        <Container justify='space-between' display='inline-flex' responsive={true}>
            <Row align='center' justify='center' css={{ borderColor: "Grey",borderBottomStyle:"solid", borderBottomWidth: "thin"}}>
                <Text h1 size={55}> Choosy </Text>
            </Row>
            <Spacer/>
            <Row>
                <Text h3> Allergy Settings</Text>
            </Row>
            <Row align='center' justify='center'>
                <Input size={'xl'} width={'100%'} placeholder={'Search'}  underlined/>
            </Row>
            <AllergyElement food={'Dairy milk'}></AllergyElement>
            <AllergyElement food={'Eggs'}></AllergyElement>
            <AllergyElement food={'Peanuts'}></AllergyElement>
            <AllergyElement food={'Soy'}></AllergyElement>
            <AllergyElement food={'Wheat'}></AllergyElement>
            <AllergyElement food={'Corn'}></AllergyElement>
            <AllergyElement food={'Gelatin'}></AllergyElement>
            <AllergyElement food={'Pork'}></AllergyElement>
            <Nav></Nav>
        </Container>
    );
}

export default Allergy;
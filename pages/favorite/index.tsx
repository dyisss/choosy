import {NextPage} from "next";
import {Grid, Card, Text, Container, Row, Spacer, Col, Image} from "@nextui-org/react";
import Nav from "../nav";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faStar} from "@fortawesome/free-solid-svg-icons";

const Favorite: NextPage = () => {
    return (
        <Container justify='space-between' display='inline-flex' responsive={true}>
            <Row justify="center" align="center">
                <Text h1 size={50}>
                    Choosy
                </Text>
            </Row>
            <Spacer/>
            <Row justify="space-around" align="center">
               <Col>
                   <FontAwesomeIcon
                       icon={faStar}
                       style={{ fontSize: 21, color: "yellowgreen" }}
                   />
                    <Image src={'https://www.dinneratthezoo.com/wp-content/uploads/2017/10/firecracker-chicken-1.jpg'}>

                    </Image>
               </Col>
                <Spacer/>
                <Col>
                    <Text>
                        Firecracker chicken
                    </Text>
                </Col>
            </Row>
            <Spacer/>
            <Row justify="space-around" align="center">
                <Col>
                    <FontAwesomeIcon
                        icon={faStar}
                        style={{ fontSize: 21, color: "yellowgreen" }}
                    />
                    <Image src={'https://img.taste.com.au/grvDBPee/taste/2016/11/parmesan-crumbed-veal-cutlets-with-sweet-potato-mash-1788-1.jpeg'}>

                    </Image>
                </Col>
                <Spacer/>
                <Col>
                    <Text>
                        Veal Cutlets
                    </Text>
                </Col>
            </Row>
            <Nav/>
        </Container>
    )
}

export default Favorite
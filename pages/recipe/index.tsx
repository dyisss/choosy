import {NextPage} from "next";
import {Grid, Card, Text, Container, Row, Spacer, Col, Image, Button} from "@nextui-org/react";
import Nav from "../nav";

// @ts-ignore
const MockItem = ({ text }) => {
    return (
        <Card color="primary" css={{ h: "$20" }}>
            <Text h6 size={15} color="white" css={{ m: 0 }}>
                {text}
            </Text>
        </Card>
    );
};

const Recipe: NextPage = () => {
    // @ts-ignore
    return (
        <Container justify='space-between' display='inline-flex' responsive={true}>
            <Row justify="center" align="center">
                <Text h1 size={60}>
                    Choosy
                </Text>
            </Row>
            <Spacer/>
            <Row>
                <Col>
                    <Row  align={'center'} justify={'center'}>
                        <Text h4 color={'$green700'}>
                            Veal Cutlets
                        </Text>
                    </Row>
                    <Row  align={'center'} justify={'center'}>
                        <Image src={'https://img.taste.com.au/grvDBPee/taste/2016/11/parmesan-crumbed-veal-cutlets-with-sweet-potato-mash-1788-1.jpeg'}/>
                    </Row>
                </Col>
                <Col>
                    <Row  align={'center'} justify={'center'}>
                        <Text>Ingredients</Text>
                    </Row>
                    <Row>
                        <Text>
                            1 pound veal cutlets
                            <br/>
                            1/2 cut all-purpose flour
                            <br/>
                            2 large eggs
                            <br/>
                            1 tablespoon water
                            <br/>
                            2 cups breadcrumbs
                            <br/>
                            Oil
                            <br/>
                            Lemon Wedges, Garnish
                        </Text>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Text h4 color={'$green700'}>
                    Recipe
                </Text>
            </Row>
            <Row>
                Gather the ingredients.
                Pat dry the cutlets and set them aside.
                On a plate, combine the flour and sale.
                In a shallow bowl, whisk th eggs thoroughly
                with the water.
                Put the breadcrumbs oon another plate.
                Line the plates and bowls in order from left to right: flour, egg, breadcrumbs.
                Set a platter or baking tray at the end of the line, after the breadcrumbs.
            </Row>
            <Row align={'center'} justify={'center'}>

                <Button> {'>'} </Button>
            </Row>
            <Nav/>
        </Container>
    )
}

export default Recipe
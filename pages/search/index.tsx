import {NextPage} from "next";
import {Grid, Card, Text, Input, Container, Row, Spacer, Col, Radio, Button, Link} from "@nextui-org/react";
import Nav from "../nav";

// @ts-ignore
const MockItem = ({ text }) => {
    return (
        <Card color="primary" css={{ h: "$20" }}>
            <Text h6 size={15} color="white" css={{ m: 0 }}>
                {text}
            </Text>
        </Card>
    );
};

const Search: NextPage = () => {
    return (
        <Container justify='space-between' display='inline-flex' responsive={true}>
            <Row justify="center" align="center">
                <Text h1 size={50}>
                    Choosy
                </Text>
            </Row>
            <Spacer/>
            <Row justify="center" align="center">
                <Input placeholder='Search'></Input>
            </Row>
            <Spacer/>
            <Container>
                <Card>
                    <Row>
                        <Text h4>
                            Filters
                        </Text>
                    </Row>
                    <Spacer></Spacer>
                    <Row justify="space-evenly">
                        <Text h5>
                            Cooking Time
                        </Text>
                        <Input placeholder='Minutes'></Input>
                    </Row>
                    <Spacer/>
                    <Row justify="space-evenly">
                        <Text h5>
                            Price per person
                        </Text>
                        <Input placeholder='Price'></Input>
                    </Row>
                    <Spacer/>
                    <Row justify="space-evenly">
                        <Text h5>
                            Number of people
                        </Text>
                        <Input placeholder='People'></Input>
                    </Row>
                    <Spacer/>
                    <Row justify="center" align="center">
                        <Text h5>
                            Ingredients
                        </Text>
                    </Row>
                    <Row justify="center" align="center">
                        <Radio.Group value="Lettuce">
                            <Row>
                                <Col>
                                    <Radio value="Lettuce">Lettuce</Radio>
                                    <Radio value="Pasta">Pasta</Radio>
                                    <Radio value="Rice">Rice</Radio>
                                </Col>
                                <Spacer/>
                                <Col>
                                    <Radio value="Chicken">Chicken</Radio>
                                    <Radio value="Paprika">Paprika</Radio>
                                    <Radio value="Tomato">Tomato</Radio>
                                </Col>
                            </Row>
                        </Radio.Group>
                    </Row>
                    <Spacer/>
                    <Row justify="center" align="center">
                        <Text h5>
                            Nutrients
                        </Text>
                    </Row>
                    <Row justify="center" align="center">
                        <Radio.Group value="HP">
                            <Row>
                                <Col>
                                    <Radio value="HP">High proteins</Radio>
                                    <Radio value="LC">Low carbs</Radio>
                                </Col>
                                <Spacer/>
                                <Col>
                                    <Radio value="HM">High minerals</Radio>
                                    <Radio value="LL">Low lipids</Radio>
                                </Col>
                            </Row>
                        </Radio.Group>
                    </Row>
                    <Row justify="center" align="center">
                        <Link href={'/recipe'}>
                            <Button>Apply</Button>
                        </Link>
                    </Row>
                </Card>
            </Container>
            <Nav></Nav>
        </Container>
    )
}

export default Search